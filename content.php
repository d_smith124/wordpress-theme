        <div class="jumbotron">
          <h1 class="display-3"><?php echo get_bloginfo('description') ?></h1>
          <p class="lead"><?php echo get_bloginfo('under_tagline'); ?></p>
          <p><a class="btn btn-lg btn-success" href="#" role="button">Sign up today</a></p>
        </div>
      </main>