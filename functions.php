<?php
add_action( 'widgets_init', 'my_register_sidebars' );
function my_register_sidebars() {
    register_sidebar(
        array(
            'id'            => 'primary',
            'name'          => __( 'Primary Sidebar' ),
            'description'   => __( 'Top left sidebar.' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );
}

$new_general_setting = new new_general_setting('under_tagline');

class new_general_setting {
    function new_general_setting( ) {
        add_filter( 'admin_init' , array( &$this , 'register_fields' ) );
    }
    function register_fields() {
        register_setting( 'general', 'under_tagline', 'esc_attr' );
        add_settings_field('under_tag', '<label for="under_tagline">'.__('Under Tagline Text' , 'under_tagline' ).'</label>' , array(&$this, 'fields_html') , 'general' );
    }
    function fields_html() {
        $value = get_option( 'under_tagline', '' );
        echo '<input type="text" id="under_tagline" name="under_tagline" value="' . $value . '" />';
    }
}