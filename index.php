<?php get_header(); ?>
  <?php get_sidebar('primary'); ?>
  <?php get_template_part( 'content', get_post_format()); ?>
<?php get_footer(); ?>